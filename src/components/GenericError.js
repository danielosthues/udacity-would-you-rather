import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class GenericError extends Component{
  render(){
    return (
      <div className='alert alert-danger' role='alert'>
        <h4 className='alert-heading'>Ups something went wrong - Error 404</h4>
          <p>Looks like you are trying to access some content that does not exist anymore</p>
          <hr/>
          <p>You might better start <Link className='navbar-brand' to={'/'}>here</Link></p>
      </div>
    )
  }
}

export default GenericError
