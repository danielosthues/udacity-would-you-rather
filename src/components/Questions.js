import React, { Component } from 'react'
import QuestionItem from './QuestionItem'
import {connect} from 'react-redux'

const SHOW_QUESTIONS_UNANSWERED = 'SHOW_QUESTIONS_UNANSWERED'
const SHOW_QUESTIONS_ANSWERED = 'SHOW_QUESTIONS_ANSWERED'

class Questions extends Component{
  state = {showQuestions : SHOW_QUESTIONS_UNANSWERED}

  handleQuestionState = (e) => {
    e.preventDefault()
    var toggle = e.target.getAttribute('data-toggle');
    if(toggle){
      this.setState(() => ({
        showQuestions : toggle
      }))
    }
  }

  render(){
    const {questionsUnAnswered,questionsAnswered} = this.props;
    const questionsUnAnsweredOrdered = Object.values(questionsUnAnswered).sort((a, b) => b.timestamp - a.timestamp).map((q) => q.id);
    const questionsAnsweredOrdered = Object.values(questionsAnswered).sort((a, b) => b.timestamp - a.timestamp).map((q) => q.id);
    return (
      <div>
        <div>
          <div className='btn-group mb-4' role='group' aria-label='Basic example'>
            <button
              onClick={this.handleQuestionState}
              type='button'
              className='btn btn-secondary'
              data-toggle={SHOW_QUESTIONS_UNANSWERED}
              id='toggleQuestionsUnAnswered'
              disabled={this.state.showQuestions === SHOW_QUESTIONS_UNANSWERED}>
              Unanswered <span className="badge badge-light">{questionsUnAnswered.length}</span>
            </button>
          <button
            onClick={this.handleQuestionState}
            type='button'
            className='btn btn-secondary'
            data-toggle={SHOW_QUESTIONS_ANSWERED}
            id='toggleQuestionsAnswered'
            disabled={this.state.showQuestions === SHOW_QUESTIONS_ANSWERED}>
            Answered <span className="badge badge-light">{questionsAnswered.length}</span>
          </button>
        </div>
        </div>
        {this.state.showQuestions === SHOW_QUESTIONS_UNANSWERED && (
          <div id='questionsUnAnswered'>
              {questionsUnAnsweredOrdered.map((id)=>(
                <QuestionItem key={id} id={id}/>
              ))}

              {questionsUnAnswered.length === 0 && (
                <div className='alert alert-success' role='alert'>
                  <h4 className='alert-heading'>Well done!</h4>
                    <p>You answered all available questions</p>
                </div>
              )}
          </div>
        )}
        {this.state.showQuestions === SHOW_QUESTIONS_ANSWERED && (
          <div id='questionsAnswered'>
              {questionsAnsweredOrdered.map((id)=>(
                <QuestionItem key={id} id={id}/>
              ))}
          </div>
        )}
      </div>
    )
  }
}

function mapStateToProps({authedUser, questions}){
    const questionsUnAnswered = Object.values(questions).filter((question) =>
        !question.optionOne.votes.includes(authedUser) && !question.optionTwo.votes.includes(authedUser))
    const questionsAnswered = Object.values(questions).filter((question) =>
        question.optionOne.votes.includes(authedUser) || question.optionTwo.votes.includes(authedUser))
    return {
        questionsUnAnswered,//: Object.values(questionsUnAnswered).sort((a, b) => b.timestamp - a.timestamp).map((q) => q.id),
        questionsAnswered//: Object.values(questionsAnswered).sort((a, b) => b.timestamp - a.timestamp).map((q) => q.id)
      }
}

export default connect(mapStateToProps)(Questions)
