import React, { Component, Fragment } from 'react'
import NavigationBarLinks from './NavigationBarLinks'
import {unsetAuthedUser} from '../actions/authedUser'
import { connect } from 'react-redux'

class NavigationBar extends Component{
  render(){
    const {user} = this.props;
    const authorized = !!user ? true : false;
    return (
      <nav className='navbar navbar-dark bg-dark mb-4 mt-4'>
        <NavigationBarLinks/>
        {(user && authorized) &&
          <Fragment>
            <div className='navbar-brand' href='#'>
              Logged in as : {user.name} <img src={user.avatarURL} width='auto' height='30px' className='d-inline-block align-top' alt=''/>
            </div>
            <button
              className='btn btn-primary'
              onClick={this.toggleLogin}>
              Logout
            </button>
          </Fragment>
        }
      </nav>
    )
  }

  toggleLogin = (e) => {
    const {dispatch, authedUser} = this.props;
    const authorized = authedUser !== null;
    if(authorized){
      dispatch(unsetAuthedUser());
      return;
    }

  }
}

function mapStateToProps({authedUser, users})
{
    const user = users[authedUser]
    return {
      user
    }
}

export default connect(mapStateToProps)(NavigationBar)
