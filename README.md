This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

# Would you rather ? Project


This project enables the user to play the “Would You Rather?” game.
The game goes like this:

A user is asked a question in the form: “Would you rather [option A] or [option B] ?”.
Answering "neither" or "both" is against the rules.

In the app, users are be able to answer questions, see which questions they haven’t answered, see how other people have voted, post questions, and see the ranking of users on the leaderboard.


## What You're Getting
```bash
├── public
│   ├── favicon.ico # React Icon, You may change if you wish.
│   └── index.html # DO NOT MODIFY
│   └── manifest.json # DO NOT MODIFY
└── src
    |──actions
    │   │──authedUser.js # actioncreators for login / logout actions
    │   │──questions.js # actions related to the questions - part of the state
    │   │──shared.js # actions related to both parts of the state ( questions, users )
    │   └──users.js # actions related to the users - part of the state
    │
    │──components
    │   │──DateRenderer.js # renders a small piece of ui displaying a date
    │   │──GenericError.js # displays a Generic 404 Error for instance when a question is no longer available
    │   │──LeaderBoard.js # displays the users in order of their score under the /leaderboard route
    │   │──LeaderBoardItem.js # represents a user within a Leaderboard
    │   │──Login.js # displays the Login UI on the home route when user is not authenticated
    │   │──NavigationBar.js # displays the Navigation UI
    │   │──NavigationBarLinks.js # represents the fragment of the NavigationBar that includes Links
    │   │──NewQuestion.js # represents the Form where the user may add a new question under the /new route
    │   │──Questions # represents the list of questions under the home route / when the user is authenticated
    │   │──QuestionItem # displays a question in the list of questions on the home - route
    │   │──QuestionDetailItem # shows up when user clicks on QuestionItem.
    │   │──QuestionDetailItemResult # is included in QuestionDetailItem, shows up when user has voted
    │   │──QuestionDetailItemVote # is included in QuestionDetailItem, shows up when user did not vote already
    │   │──Routes # List of available routes including catch all for not defined routes
    │   └──User # UI - representation of a User on Login.js
    │   
    │──middleware
    │   │──index.js # sums up all middleware included as a central hub to be included
    │   └──logger.js # logs state to the console
    │
    │──reducers
    │     │──authedUser.js # reducer for login / logout handling
    │     │──index.js # sums up all reducers included as a central hub to be included
    │     │──questions.js # reducers related to the questions - part of the state
    │     └──users.js # reducers related to the users - part of the state
    │
    │──utils
    │   │──Data.js # Datastructure given by Udacity
    │   │──api.js # a small api build on top of _Data.js
    │   └──helpers.js # includes some helper functionality being re-used across the app
    │
    ├── .gitignore # gitignore list It's unlikely that you'll need to modify this.
    ├── package.json # npm package manager file. It's unlikely that you'll need to modify this.
    └── README.md # This file.
    ```
