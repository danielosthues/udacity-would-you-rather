import React, { Component, Fragment} from 'react'
import { Route, Switch } from 'react-router-dom'
import Questions from './Questions'
import QuestionDetailItem from './QuestionDetailItem'
import NewQuestion from './NewQuestion'
import LeaderBoard from './LeaderBoard'
import GenericError from './GenericError'

class Routes extends Component{
  render(){
    return (
            <Fragment>
              <Switch>
                <Route path='/' exact component={Questions} />
                <Route path='/questions/:id' component={QuestionDetailItem} />
                <Route path='/add' exact component={NewQuestion} />
                <Route path='/leaderboard' exact component={LeaderBoard} />
                <Route component={GenericError} />
              </Switch>
            </Fragment>
    )
  }
}

export default Routes
