import React, { Component, Fragment} from 'react'
import { Link } from 'react-router-dom'

class NavigationBarLinks extends Component{
  render(){
    return (
      <div>
          <Link className='navbar-brand' to={'/'}>Home</Link>
          <Fragment>
            <Link className='navbar-brand' to={'/add'}>New</Link>
            <Link className='navbar-brand' to={'/leaderboard'}>LeaderBoard</Link>
          </Fragment>
      </div>
    )
  }
}

export default NavigationBarLinks
