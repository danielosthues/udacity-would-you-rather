import React, { Component } from 'react'
import {connect} from 'react-redux'
import User from './User'
import { setAuthedUser } from '../actions/authedUser'

class Login extends Component{
  render(){
    return (
      <div className='card pb-2 pl-2 pr-2 pt-2' >
        <h3 className='card-header'>
          Login
        </h3>
        <div className='card-body'>
          <h4>In order to use this application you must be authenticated.</h4>
          <p>Please pick up a user to login</p>
        </div>
        <div className="card-group">
          {this.props.userIds.map((id)=>(
            <User
              id={id}
              key={id}
              handleLogin={this.handleLogin}/>
          ))}
      </div>
    </div>
    )
  }

  handleLogin = (userId) => {
    const {dispatch} = this.props;
    dispatch(setAuthedUser(userId));
  }
}

function mapStateToProps({users}){
  return {
    userIds : Object.keys(users)
  }
}

export default connect(mapStateToProps)(Login)
